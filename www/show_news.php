<?php 

	$connect = db_connect();
	$get_id = $_GET['id'];
	$sql = 'SELECT * FROM news WHERE id=:id';
	$stml = $connect->prepare($sql);
	$stml->execute(array(':id' => $get_id));
	$stml->bindColumn('author', $content['autor']);
	$stml->bindColumn('head', $content['head']);
	$stml->bindColumn('text', $content['text']);
	$stml->fetch(PDO::FETCH_ASSOC);

	if (isset($_POST['send_rating'])){
		$rate = $_POST['rating'];
		$db_rate_connect = db_connect();
		$sql_rate = 'INSERT INTO rating VALUES (:id, :post_id, :value, :author)';
		$stml_r = $db_rate_connect->prepare($sql_rate);
		$stml_r->execute(array(':id' => '',
			':post_id' => $_GET['id'],
			':value' => $rate,
			':author' => $_SESSION['login']));
	}
?>


<div class='news-wrapper'>
		<div class='news'>
			<div class='head-news'>
				<h2><?php echo"{$content['head']}"; ?></h2>
				<div class='p'><p><?php echo"{$content['autor']}"; ?> | За цей матеріал ще ніхто не голосував</p><hr></div>
			</div>
			<div class='content-news'>
				<p><?php echo"{$content['text']}"; ?></p>
			</div>
		</div>
		<hr>
</div>
<?php
echo 'Ваша оцінка' . getRate($_SESSION['login']) . ' | Середня оцінка' . getAllRate($get_id);
	if (isset($_SESSION['id'])){
		echo '<a href="?page=update_news&id='.$get_id.'"> <input class="button" type="submit" value="Редагувати"></a>
		<hr>';
		if (checkRate($_SESSION['login'])){
			echo 'Ваша оцінка' . getRate($_SESSION['login']) . ' | Середня оцінка' . getAllRate($get_id);
		} else {
			echo '<form action="" method="post" id="rating">
				<p>
					Оцінка за матеріал
				</p>
				1 <input type="radio" name="rating" value="1">
				2 <input type="radio" name="rating" value="2">
				3 <input type="radio" name="rating" value="3">
				4 <input type="radio" name="rating" value="4">
				5 <input type="radio" name="rating" value="5"><br>
				<input type="submit" name="send_rating" class="button" value="Оцінити">
			</form>';
		}


			echo '<form action="" method="post" id="comment">
				<label for="add-comment">Додати коментар</label><br>
				<textarea name="add-comment" id="" cols="80" rows="10"></textarea><br>
				<input type="submit" name="send_comment" class="button" value="Відправити">
			</form>';
	}
?>

<hr>
<div class="comments">
	<div class="comment-wrapper">
		<div class="comment">
			<h4>Author</h4>
			<p>Text comment</p>
		</div>
	</div>
	<div class="comment-wrapper">
		<div class="comment">
			<h4>Author</h4>
			<p>Text comment</p>
		</div>
	</div>
	<div class="comment-wrapper">
		<div class="comment">
			<h4>Author</h4>
			<p>Text comment</p>
		</div>
	</div>
	<div class="comment-wrapper">
		<div class="comment">
			<h4>Author</h4>
			<p>Text comment</p>
		</div>
	</div>
</div>


<?php $connect = NULL; ?>