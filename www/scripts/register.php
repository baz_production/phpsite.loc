<?php
	setlocale(LC_ALL, "");
	require_once "functions.php";
	$connect = db_connect();

	if(isset($_POST['submit'])){
		$login = trim($_POST['username']);
		$pass = trim($_POST['passwd']);
		$r_pass = trim($_POST['r_passwd']);
		$mail = trim($_POST['mail']);

		$true_mail_dog = preg_match("/@/", $mail);
		$true_mail_dot = preg_match("/(.com|.ru|.ua|.org)/", $mail);
		
		$sql = "SELECT name FROM users WHERE name= :login";
		$stml = $connect->prepare($sql);
		$stml->bindValue(':login', $login);
		$stml->execute();
		$stml->bindColumn('name', $db_login);
		$stml->fetch();
		
		if (!$db_login) {
			
			if($pass == $r_pass){
				
				if ($true_mail_dot && $true_mail_dog) {
					$pass = md5($pass);
					$sql = "INSERT INTO users VALUES (:id, :login, :mail, :pass, :author)";
					$stml = $connect->prepare($sql);
					$stml->execute(array(':id'=>'',
										 ':login'=>$login,
										 ':mail'=>$mail,
										 ':pass'=>$pass,
										 ':author'=>'user'));
					header('Location: login.php?l='.$login.'&p='.$pass);
				} else { header('Location: ../register.php'); }
			} else { header('Location: ../register.php'); }
		} else { header('Location: ../register.php'); }
	}
	$connect = NULL;
?>