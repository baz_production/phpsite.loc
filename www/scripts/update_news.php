<?php 
	session_start();
	setlocale(LC_ALL, "");
	require_once "functions.php";

	if (isset($_POST['submit'])) {
		$head = trim($_POST['head']);
		$text = trim($_POST['text']);
		$get_id = $_GET['id'];

		$sql = "UPDATE news 
				SET head=:head, text=:text 
				WHERE id=$get_id";

		$db = db_connect();
		$stml = $db->prepare($sql);
		$stml->execute(array(':head'  => $head,
							 ':text'  => $text));
		$db = NULL;

		header('Location: ../index.php');
	}
 ?>