<?php 
	session_start();
	setlocale(LC_ALL, "");
	require_once "functions.php";

	if (isset($_POST['submit'])) {
		$autor = trim('admin');
		$head = trim($_POST['head']);
		$text = trim($_POST['text']);

		$sql = "INSERT INTO news VALUES(:id, :autor, :head, :text)";
		$db = db_connect();
		$stml = $db->prepare($sql);
		$stml->execute(array(':id'    => '',
							 ':autor' => $autor,
							 ':head'  => $head,
							 ':text'  => $text));
		$db = NULL;

		header('Location: ../index.php');
	}
 ?>