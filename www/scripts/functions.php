<?php
	setlocale(LC_ALL, "");

	// Menu
	function show_header(){
		if (isset($_SESSION['login'])){
			echo '<div class="header">
				<ul id="navigation">
					<li><a href="index.php">Головна</a></li>
					<li><a href="index.php?page=user_info&id='.$_SESSION['id'].'">Про мене</a></li>
					<li><a href="index.php?page=all_users">Всі користувачі</a></li>
					<li><a href="index.php?page=add_news">Додати новину</a></li>
				</ul>
			</div>';
		} else {
			echo '<div class="header">
				<ul id="navigation">
					<li><a href="index.php">Головна</a></li>
					<li><a href="index.php?page=all_users">Всі користувачі</a></li>
					<!--<li><a href="add_news.php">Додати новину</a></li>-->
					<li class="login_active">
						<a href="?page=login">Вхід</a>
						<div class="login_wrapper">
							<div class="arr"></div>
							<div class="login">
								<form id="form_login" action="scripts/login.php" method="post">
									<input class="input" type="text" name="login" placeholder="Логін" required><br>
									<input class="input" type="password" name="pass" placeholder="Пароль" required><br>
									<input class="button" type="submit" name="enter" value="Вхід">
								</form>
								<a href="index.php?page=register">Реєстрація</a>
							</div>
						</div>
					</li>
				</ul>
			</div>';
		}
	}

	function content($page){
		switch($page){
			case 'register': include('register.php'); break;
			case 'add_news': include('add_news.php'); break;
			case 'all_users': include('all_users.php'); break;
			case 'show_news': include('show_news.php'); break;
			case 'update_news': include('update_news.php'); break;
			case 'update_user': include('update_user.php'); break;
			case 'login': include('login.php'); break;
			case 'user_info': include('user_info.php'); break;
			//case '': include('.php'); break;
			default: include ('main.php');
		}
	}

	function db_connect(){
		$log = "root";
		$pass = "Arhange12";
		try { $pdo_db = new PDO("mysql:dbname=phpsite.loc;host=localhost;", $log, $pass);} 
		catch (PDOException $e) { echo 'Помилка з’єднання з БД '.$e; exit;}
		return $pdo_db;
	}

	function getAllUsers(){
		$db = db_connect();
		$sql = "SELECT * FROM users";
		$res = $db->prepare($sql);
		$res->execute();
		$res->bindColumn('id', $id);
		$res->bindColumn('name', $name);
		echo "<ul>";
		while ($res->fetch(PDO::FETCH_ASSOC)){ 
			echo "<li><a href ='?page=user_info&id={$id}'>{$name}</a></li><br>";
		}
		echo "</ul>";
	}

	function showUser ($id){
		$db = db_connect();
		$sql = "SELECT * FROM users WHERE id= :id";
		$stml = $db->prepare($sql);
		$stml->bindValue(':id', $id);
		$stml->execute();

		$stml->bindColumn('id', $usr_id);
		$stml->bindColumn('name', $name);
		$stml->bindColumn('mail', $mail);
		$stml->fetch();

		echo "<br><table>";
		echo "<tr><td>ID: </td><td>".$usr_id."</td></tr>";
		echo "<tr><td>Логін: </td><td>".$name."</td></tr>";
		echo "<tr><td>E-mail: </td><td>".$mail."</td></tr>";
		echo "</table>";

		if (isset($_SESSION['id'])){
			echo '<a href="scripts/exit.php"><input type="submit" class="button" value="Вихід"></a>';
			echo '<a href="?page=update_user&id='.$_SESSION['id'].'">';
			echo '<input type="submit" class="button" value="Редагувати"></a>';
		}
		$db = NULL;
	}

	function post_news(){
		$db = db_connect();
		$sql = 'SELECT * FROM news';
		$stml = $db->prepare($sql);
		$stml->execute();
		$content = array();
		$stml->bindColumn('id', $content['id']);
		$stml->bindColumn('head', $content['head']);
		$stml->bindColumn('text', $content['text']);
		$stml->fetch();

		while ($stml->fetch(PDO::FETCH_ASSOC)) {
			echo "<div class='news-wrapper'>
					<div class='news'>
						<div class='head-news'>
							<a href='?page=show_news&id=".$content['id']."'><h2>". $content['head'] ."</h2></a>
							<div class='p'>За цей матеріал ще ніхто не голосував<hr></div>
						</div>
							<div class='content-news'>
							<p>". $content['text'] ."</p>
							</div>
						</div>
						<hr>
						<div style='clear:both'></div>
					</div> ";
		}
		$db = NULL;
	}

	function checkRate($login){
		$author = NULL;
		$db = db_connect();
		$sql = 'SELECT * FROM rating WHERE :author';
		$stml = $db->prepare($sql);
		$stml -> execute(array(':author' => $login));
		$stml->bindColumn('author', $author);
		$stml->fetch(PDO::FETCH_ASSOC);
		$db = NULL;
		echo $author;
		if ($author) return true;
		else return false;
	}

	function getRate($login){
		$db = db_connect();
		$sql = 'SELECT * FROM rating WHERE :login';
		$stml = $db->prepare($sql);
		$stml -> execute(array(':login' => $login));
		$stml -> bindColumn('value', $v);
		$stml->fetch(PDO::FETCH_ASSOC);
		$db = NULL;
		return $v;
	}

	function getAllRate($post_id){
		$db = db_connect();
		$sql = 'SELECT (id, value) FROM rating WHERE :post_id';
		$stml = $db->prepare($sql);
		$stml -> execute(array(':post_id' => $post_id));
		$stml->bindValue('value', $val);
		$stml->fetch();
		if (count($stml) > 1){
			$s = array_sum($stml);
			$c = count($stml);
			$v = $s / $c;
		}	else $v = $val;
		$db = NULL;
		return $v;
	}

 ?>
