<?php 
	session_start();
	setlocale(LC_ALL, "");
	require_once "functions.php";	
	$connect = db_connect();
	$get_id = $_GET['id'];
	$sql = 'DELETE FROM news WHERE id=:id';
	$stml = $connect->prepare($sql);
	$stml->execute(array(':id' => $get_id));

	header("Location: ../index.php");
	$connect = NULL;
	exit();
?>