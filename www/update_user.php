<?php 

	$id = $_SESSION['id'];

	if (isset($id)){
		if (isset($_POST['del'])){
			$db = db_connect();
			$sql = "DELETE FROM users WHERE id=:id";
			$stml = $db->prepare($sql);
			$stml->execute(array(':id'=>$id)) or die (PDOException);
			require_once 'scripts/exit.php';
			header("Location: index.php");
			$db = NULL;
			exit();
		}

		if (isset($_POST['log'])){
			$db = db_connect();
			$sql = "UPDATE users SET name=:login WHERE id=:id";
			$stml = $db->prepare($sql);
			$stml->execute(array(':login' => trim($_POST['login']),
								 ':id'    => $id)) or die (PDOException);
			$db = NULL;
		}

		if (isset($_POST['button_pass'])){
			$db = db_connect();
			$sql = 'SELECT pass FROM users WHERE id=:id';
			$stml = $db->prepare($sql);
			$stml->execute(array(':id' => $id));
			$stml->bindColumn('pass', $u_pass);

			$pass = trim($_POST['pass']);
			$r_pass = trim($_POST['rpass']);

			if ($pass == $r_pass){
				$db = db_connect();
				$sql = "UPDATE users SET pass=:pass WHERE id=:id";
				$stml = $db->prepare($sql);
				$pass = md5($pass);
				$stml->execute(array(':pass' => $pass,
									 ':id'   => $id)) or die (PDOException);
				$db = NULL;
			} else {	echo 'Passwords must mutch!';	}
		}
	} else {	header("Location: index.php");$db = NULL;	}
 ?>


<h2></h2>
<form action="update_user.php" method="post">
	<table>
		<tr>
			<td>
				<label for="login">Новий логін</label>
			</td>
			<td>
				<input type="text" name="login">
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" name="log" class="button" value="Прийняти">
			</td>
		</tr>
	</table>
</form>
<hr>
<form action="update_user.php" method="post">
	<table>
		<tr>
			<td><label for="pass">Новий пароль</label></td>
			<td><input type="password" name="pass"></td>
		</tr>
		<tr>
			<td><label for="rpass">Повторіть новий пароль</label></td>
			<td><input type="password" name="rpass"></td>
		</tr>
		<tr>
			<td><input type="submit" name="button_pass" class="button" value="Прийняти"></td>
		</tr>
	</table>
</form>

	<form action="update_user.php" method="post">
		<input type="submit" name="del" class="button" value="Видалити аккаунт">
	</form>
