<?php  
	session_start();
	setlocale(LC_ALL, ""); 
	require_once "scripts/functions.php";
?>

<!DOCTYPE html>
<html lang='ua'>
	<head>
		<meta charset="utf-8">
		<title>Головна</title>
		<link rel="stylesheet" href="css/style_2.css" charset="utf-8">
	</head>
	
	<body>
		<div class="wrapper">
			<?php show_header(); ?>

			<div class="content">
				<?php
					if(isset($_GET['page'])){
						content($_GET['page']);
					} else {
						$_GET['page'] = 'main';
						content($_GET['page']);
					}
				?>
			</div>
		</div>
	</body>
</html>

